package com.beer.jeeprojectsdbm.jaas;

import com.beer.jeeprojectsdbm.service.ContextBean;
import jakarta.annotation.PostConstruct;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.io.Serializable;

/**
 * Sample login bean for JSF
 */
@Named("loginBean")
@ViewScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    private String requete;
    @Inject
    FacesContext facesContext;
    @Inject
    ExternalContext externalContext;
    @Inject
    ContextBean contextBean;

    private String originalURI;
    private String originalQuery;

    @PostConstruct
    public void init() {
        originalURI = (String) externalContext.getRequestMap().get(RequestDispatcher.FORWARD_REQUEST_URI);

        if (originalURI == null)
            originalURI = externalContext.getApplicationContextPath() + "/faces/index.xhtml";

        originalQuery = (String) externalContext.getRequestMap().get(RequestDispatcher.FORWARD_QUERY_STRING);
        originalURI = originalURI.substring(originalURI.indexOf("/faces"));
        if (originalQuery != null) {
            originalURI += "?" + originalQuery;
        }
    }

    public String login() {

        try {
            // Get the current servlet request from the ExternalContext
            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            // Do login from the container (will call login module)
            request.login(username, password);

            if(originalQuery != null)
                originalURI += (originalQuery.isEmpty() ? "?faces-redirect=true" : "&faces-redirect=true");

            return originalURI;
        } catch ( ServletException  servletException ) {
            //servletException.printStackTrace();
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Une erreur s'est produite : Login failed", null));
           facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Une erreur s'est produite : Login failed", null));
            //Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, servletException);
            return "/faces/login/error.xhtml";
        }

    }
    public String logout() {
        try {
            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            request.logout();

            return "/faces/login/login.xhtml";
        } catch (ServletException e) {
            return "/faces/login/error.xhtml";
            //throw new RuntimeException(e);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
