package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.Pays;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter(value = "paysConverter", managed = true)
public class PaysConverter implements Converter {
    @Inject
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s != null && s.trim().length() > 0) {
            for (Pays pays : bean.getListCountries()) {
                if (pays.getId() == Integer.parseInt(s)) {
                    bean.setSelectedContinent(pays.getContinent());
                    return pays;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Pays pays = (Pays) object;
        return String.valueOf(pays.getId());
    }
}
