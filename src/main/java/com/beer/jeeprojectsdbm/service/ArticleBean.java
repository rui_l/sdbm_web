package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.dao.DaoFactory;
import com.beer.jeeprojectsdbm.metier.*;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

import java.io.Serializable;
import java.util.List;

@SessionScoped
@Named(value = "articleBean")
public class ArticleBean implements Serializable {
    private List<TypeBiere> allTypes;
    private TypeBiere selectedType;
    private List<Couleur> allColors;
    private Couleur selectedColor;
    private List<Continent> allContinents;
    private Continent selectedContinent;
    private List<Pays> listCountries;
    private Pays selectedCountry;
    private List<Fabricant> allMakers;
    private Fabricant selectedMaker;
    private List<Marque> listBrands;
    private Marque selectedBrand;

    private String keywordForArticleLibelle;
    private ArticleSearch articleSearch;
    private List<Article> listArticles;

    @PostConstruct
    private void init() {
        this.allTypes = DaoFactory.getTypeDAO().getAll();
        this.allTypes.add(0, new TypeBiere(0, "- Choisir un type de bière -"));
        this.allColors = DaoFactory.getCouleurDAO().getAll();
        this.allColors.add(0, new Couleur(0, "- Choisir une couleur -"));
        this.allContinents = DaoFactory.getContinentDAO().getAll();
        this.allContinents.add(0, new Continent(0,"- Choisir un continent -"));
        this.listCountries = DaoFactory.getPaysDAO().getAll();
        this.listCountries.add(0, new Pays(0, "- Choisir un pays -"));
        this.allMakers = DaoFactory.getFabricantDAO().getAll();
        this.allMakers.add(0, new Fabricant(0, "- Choisir un fabricant -"));
        this.listBrands = DaoFactory.getMarqueDAO().getAll();
        this.listBrands.add(0, new Marque(0, "- Choisir une marque -"));

        this.articleSearch = new ArticleSearch();
//        this.articleSearch.setLgPage(50);
        this.listArticles = DaoFactory.getArticleDAO().getLike(this.articleSearch);
    }

    public void reset() {
        selectedContinent = null;
        selectedCountry = null;
        selectedType = null;
        selectedColor = null;
        selectedMaker = null;
        selectedBrand = null;
        init();
    }

    public void search() {
        this.articleSearch = new ArticleSearch();
        if (keywordForArticleLibelle !=null && (! keywordForArticleLibelle.trim().equals("")))
            this.articleSearch.setLibelle(keywordForArticleLibelle);
        if (selectedContinent !=null && selectedContinent.getId() !=0) {
            this.articleSearch.setContinent(selectedContinent);
            if (this.selectedContinent.getId() == 0)
                this.selectedContinent = null;
        }
        if (selectedCountry !=null && selectedCountry.getId() !=0) {
            this.articleSearch.setPays(selectedCountry);
            if (this.selectedCountry.getId() == 0)
                this.selectedCountry = null;
        }
        if (selectedType !=null && selectedType.getId() !=0) {
            this.articleSearch.setTypeBiere(selectedType);
            if (this.selectedType.getId() == 0)
                this.selectedType = null;
        }
        if (selectedColor !=null && selectedColor.getId() !=0) {
            this.articleSearch.setCouleur(selectedColor);
            if (this.selectedColor.getId() == 0)
                this.selectedColor = null;
        }
        if (selectedMaker !=null && selectedMaker.getId() !=0) {
            this.articleSearch.setFabricant(selectedMaker);
            if (this.selectedMaker.getId() == 0)
                this.selectedMaker = null;
        }
        if (selectedBrand !=null && selectedBrand.getId() !=0) {
            this.articleSearch.setMarque(selectedBrand);
            if (this.selectedBrand.getId() == 0)
                this.selectedBrand = null;
        }
        this.listArticles = DaoFactory.getArticleDAO().getLike(this.articleSearch);
    }

    public void onTypeChange() {
        if (selectedType.getId() == 0) {
            this.selectedType = null;
        }
    }

    public void onColorChange() {
        if (selectedColor.getId() == 0) {
            this.selectedColor = null;
        }
    }

    public void onContinentChange() {
        if (selectedContinent.getId() != 0) {
            this.listCountries = DaoFactory.getPaysDAO().getByContinent(selectedContinent);
        } else {
            this.selectedContinent = null;
            this.listCountries = DaoFactory.getPaysDAO().getAll();
        }
        this.selectedCountry = null;
        this.listCountries.add(0, new Pays(0, "Choisir un pays"));
    }

    public void onCountryChange() {
        if (selectedCountry.getId() != 0) {
            if (selectedMaker != null && selectedMaker.getId() != 0) {
                this.listBrands = DaoFactory.getMarqueDAO().getByCountryAndMaker(selectedCountry, selectedMaker);
            } else {
                this.listBrands = DaoFactory.getMarqueDAO().getByCountry(selectedCountry);
            }

        } else {
            this.selectedCountry = null;
            this.listBrands = DaoFactory.getMarqueDAO().getAll();
        }
        this.selectedBrand = null;
        this.listBrands.add(0, new Marque(0, "Choisir une marque"));
    }

    public void onMakerChange() {
        if (selectedMaker.getId() !=0) {
            if (selectedCountry != null && selectedCountry.getId() !=0) {
                this.listBrands = DaoFactory.getMarqueDAO().getByCountryAndMaker(selectedCountry, selectedMaker);
            }
            this.listBrands = DaoFactory.getMarqueDAO().getByMaker(selectedMaker);
        } else {
            this.selectedMaker = null;
            if (selectedCountry.getId() == 0) {
                this.listBrands = DaoFactory.getMarqueDAO().getAll();
                this.listBrands.add(0, new Marque(0, "Choisir une marque"));
            }
        }
        this.selectedBrand = null;
    }

    public void  onBrandChange() {
        if (selectedBrand.getId() == 0) {
            this.selectedBrand = null;
        }
    }


    public List<TypeBiere> getAllTypes() {
        return allTypes;
    }

    public void setAllTypes(List<TypeBiere> allTypes) {
        this.allTypes = allTypes;
    }

    public TypeBiere getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(TypeBiere selectedType) {
        this.selectedType = selectedType;
    }

    public List<Couleur> getAllColors() {
        return allColors;
    }

    public void setAllColors(List<Couleur> allColors) {
        this.allColors = allColors;
    }

    public Couleur getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(Couleur selectedColor) {
        this.selectedColor = selectedColor;
    }

    public List<Continent> getAllContinents() {
        return allContinents;
    }

    public void setAllContinents(List<Continent> allContinents) {
        this.allContinents = allContinents;
    }

    public Continent getSelectedContinent() {
        return selectedContinent;
    }

    public void setSelectedContinent(Continent selectedContinent) {
        this.selectedContinent = selectedContinent;
    }

    public List<Pays> getListCountries() {
        return listCountries;
    }

    public void setListCountries(List<Pays> listCountries) {
        this.listCountries = listCountries;
    }

    public Pays getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(Pays selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public List<Fabricant> getAllMakers() {
        return allMakers;
    }

    public void setAllMakers(List<Fabricant> allMakers) {
        this.allMakers = allMakers;
    }

    public Fabricant getSelectedMaker() {
        return selectedMaker;
    }

    public void setSelectedMaker(Fabricant selectedMaker) {
        this.selectedMaker = selectedMaker;
    }

    public List<Marque> getListBrands() {
        return listBrands;
    }

    public void setListBrands(List<Marque> listBrands) {
        this.listBrands = listBrands;
    }

    public Marque getSelectedBrand() {
        return selectedBrand;
    }

    public void setSelectedBrand(Marque selectedBrand) {
        this.selectedBrand = selectedBrand;
    }

    public ArticleSearch getArticleSearch() {
        return articleSearch;
    }

    public void setArticleSearch(ArticleSearch articleSearch) {
        this.articleSearch = articleSearch;
    }

    public List<Article> getListArticles() {
        return listArticles;
    }

    public void setListArticles(List<Article> listArticles) {
        this.listArticles = listArticles;
    }

    public String getKeywordForArticleLibelle() {
        return keywordForArticleLibelle;
    }

    public void setKeywordForArticleLibelle(String keywordForArticleLibelle) {
        this.keywordForArticleLibelle = keywordForArticleLibelle;
    }
}
