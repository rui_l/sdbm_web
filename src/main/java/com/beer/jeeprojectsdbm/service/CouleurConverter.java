package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.Couleur;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter(value = "couleurConverter", managed=true)
public class CouleurConverter implements Converter {
    @Inject //annotation pour accéder à couleurBean
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value != null && value.trim().length() > 0) {
            for (Couleur color : bean.getAllColors()) {
                if (color.getId() == Integer.parseInt(value)) {
                    return color;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Couleur color = (Couleur) object;
        return String.valueOf(color.getId());
    }
}
