package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.Fabricant;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter (value = "fabricantConverter", managed = true )
public class FabricantConverter implements Converter {
    @Inject
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s !=null && s.trim().length() >0) {
            for (Fabricant maker:bean.getAllMakers()) {
                if (maker.getId() == Integer.parseInt(s)) {
                    return maker;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Fabricant maker = (Fabricant) object;
        return String.valueOf(maker.getId());
    }
}
