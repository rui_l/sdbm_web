package com.beer.jeeprojectsdbm.service;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.ExternalContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import java.io.Serializable;

@SessionScoped
@Named
public class ContextBean implements Serializable {
    private String pathToFaces;

    @Inject
    ExternalContext externalContext;

    @PostConstruct
    public void init() {
        pathToFaces = externalContext.getApplicationContextPath();
        pathToFaces += "/faces";
    }

    public String getPathToFaces() {
        return pathToFaces;
    }

    public void setPathToFaces(String pathToFaces) {
        this.pathToFaces = pathToFaces;
    }
}
