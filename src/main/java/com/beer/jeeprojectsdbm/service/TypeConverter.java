package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.TypeBiere;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter(value = "typeConverter", managed = true)
public class TypeConverter implements Converter {
    @Inject
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value != null && value.trim().length() > 0) {
            for (TypeBiere typeBiere : bean.getAllTypes()) {
                if (typeBiere.getId() == Integer.parseInt(value)) {
                    return typeBiere;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        TypeBiere typeBiere = (TypeBiere) object;
        return String.valueOf(typeBiere.getId());
    }
}
