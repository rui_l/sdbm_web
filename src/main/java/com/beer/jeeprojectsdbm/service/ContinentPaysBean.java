package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.dao.DaoFactory;
import com.beer.jeeprojectsdbm.metier.Continent;
import com.beer.jeeprojectsdbm.metier.Pays;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

import java.io.Serializable;
import java.util.List;

@SessionScoped
@Named (value = "cpBean")
public class ContinentPaysBean implements Serializable {
    private List<Continent> allContinents;
    private Continent selectedContinent;
    private List<Pays> listPaysContinent;
    private Pays selectedPays;

    @PostConstruct
    private void init() {
        this.allContinents = DaoFactory.getContinentDAO().getAll();
//        this.allContinents.add(0, new Continent(0,"Choisir un continent"));
        this.listPaysContinent = DaoFactory.getPaysDAO().getAll();
//        this.listPaysContinent.add(0, new Pays(0, "Choisir un pays"));
    }

    public List<Continent> getAllContinents() {
        return allContinents;
    }

    public void setAllContinents(List<Continent> allContinents) {
        this.allContinents = allContinents;
    }

    public Continent getSelectedContinent() {
        return selectedContinent;
    }

    public void setSelectedContinent(Continent selectedContinent) {
        this.selectedContinent = selectedContinent;
    }

    public List<Pays> getListPaysContinent() {
        return listPaysContinent;
    }

    public void setListPaysContinent(List<Pays> listPaysContinent) {
        this.listPaysContinent = listPaysContinent;
    }

    public Pays getSelectedPays() {
        return selectedPays;
    }

    public void setSelectedPays(Pays selectedPays) {
        this.selectedPays = selectedPays;
    }

    public void onContinentChange() {
        if (selectedContinent.getId() != 0) {
            this.listPaysContinent = DaoFactory.getPaysDAO().getByContinent(selectedContinent);
        } else {
            this.selectedContinent = null;
            this.listPaysContinent = DaoFactory.getPaysDAO().getAll();
        }
        this.selectedPays = null;
        this.listPaysContinent.add(0, new Pays(0, "Choisir un pays"));
    }

    public void onPaysChange() {
        if (selectedPays.getId() == 0) {
            this.selectedPays = null;
        }
    }
}
