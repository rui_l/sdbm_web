package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.dao.DaoFactory;
import com.beer.jeeprojectsdbm.metier.Couleur;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.tagcloud.DefaultTagCloudItem;
import org.primefaces.model.tagcloud.DefaultTagCloudModel;
import org.primefaces.model.tagcloud.TagCloudItem;
import org.primefaces.model.tagcloud.TagCloudModel;

import java.util.List;

@RequestScoped
@Named
public class TagCloudView {
    private TagCloudModel model;
    private List<Couleur> allColors;

    @PostConstruct
    public void init() {
        model = new DefaultTagCloudModel();
        allColors = DaoFactory.getCouleurDAO().getAll();
        for (Couleur color: allColors) {
            if (color.getId() % 2 ==0) {
                model.addTag(new DefaultTagCloudItem(color.getLibelle(), color.getId()));
            } else {
                model.addTag(new DefaultTagCloudItem(color.getLibelle(), "#", color.getId()));
            }
        }
    }

    public TagCloudModel getModel() {
        return model;
    }

    public void onSelect(SelectEvent<TagCloudItem> event) {
        TagCloudItem item = event.getObject();
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Couleur choisie", item.getLabel());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
