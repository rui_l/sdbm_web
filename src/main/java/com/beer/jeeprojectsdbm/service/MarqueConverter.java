package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.Fabricant;
import com.beer.jeeprojectsdbm.metier.Marque;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter(value = "marqueConverter", managed = true)
public class MarqueConverter implements Converter {
    @Inject
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s != null && s.trim().length() >0) {
            for (Marque brand : bean.getListBrands()) {
                if (brand.getId() == Integer.parseInt(s)) {
                    bean.setSelectedMaker(brand.getFabricant());
                    bean.setSelectedCountry(brand.getPays());
                    return brand;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Marque brand = (Marque) object;
        return String.valueOf(brand.getId());
    }
}
