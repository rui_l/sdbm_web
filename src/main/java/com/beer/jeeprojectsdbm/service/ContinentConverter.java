package com.beer.jeeprojectsdbm.service;

import com.beer.jeeprojectsdbm.metier.Continent;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Inject;

@FacesConverter(value = "continentConverter", managed = true)
public class ContinentConverter implements Converter {
    @Inject
    private ArticleBean bean;
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s != null && s.trim().length() > 0) {
            for (Continent continent : bean.getAllContinents()) {
                if (continent.getId() == Integer.parseInt(s)) return continent;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Continent continent = (Continent) object;
        return String.valueOf(continent.getId());
    }
}
