package com.beer.jeeprojectsdbm.security;

import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String originalPassword = "password";
        String hashedPassword = HashPassword.generate(originalPassword);
        System.out.println("Passeword hashed : " + hashedPassword);

        String passwordToCompare = "MyPassword";
        System.out.println(passwordToCompare + " : " + HashPassword.validate(passwordToCompare, hashedPassword));
        System.out.println(originalPassword + " : " + HashPassword.validate(originalPassword, hashedPassword));


        System.out.println("----------Argon2----------");
        String hashedPasswordArgon = Argon2.hash(originalPassword);
        System.out.println("Password hashed utilisant Argon2: " + hashedPasswordArgon);
        String mdpTest = "jeTesteMonMotDePasse";
        System.out.println(originalPassword+": "+ Argon2.verify(originalPassword,hashedPasswordArgon));
        System.out.println(mdpTest+": "+ Argon2.verify(mdpTest, hashedPasswordArgon));


        System.out.println("=====Argon2 génère mdp haché=====");
        String passwordUser1 = "genPassword2You";
        String hashedPasswordUser1 = Argon2.hash(passwordUser1);
        String passwordAdmin1 = "!manage4U";
        String hashedOasswordAdmin1 = Argon2.hash(passwordAdmin1);
        System.out.println("user1-mdp | "+hashedPasswordUser1+" | longueur "+hashedPasswordUser1.length());
        System.out.println("admin-mdp | "+hashedOasswordAdmin1+" | longueur "+hashedOasswordAdmin1.length());

    }
}
