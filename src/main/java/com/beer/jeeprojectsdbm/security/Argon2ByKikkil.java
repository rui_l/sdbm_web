package com.beer.jeeprojectsdbm.security;

import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

public class Argon2ByKikkil {

    public static void main(String[] args) {
        String originalPassword = "password";

        String mdpTest = "jeTesteMonMotDePasse";

        Argon2PasswordEncoder arg2SpringSecurity = new Argon2PasswordEncoder(16, 32, 1, 60000, 10);

        String springBouncyHash = arg2SpringSecurity.encode(mdpTest);
        String springBouncyHash2 = arg2SpringSecurity.encode(originalPassword);

        System.out.println(arg2SpringSecurity.matches(mdpTest,springBouncyHash));
        System.out.println(arg2SpringSecurity.matches(mdpTest,springBouncyHash2));

    }
}
