package com.beer.jeeprojectsdbm.security;

import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

public class Argon2 {
    private static final int SALT_LENGTH = 16;
    private static final int HASH_LENGTH = 32;
    private static final int PARALLELISM_INSTANCE = 1;
    private static final int MEMORY_LENGTH = 2^16;
    private static final int ITERATION = 10;

    private static final String ALGO = "$argon2id$v=19$m=" + MEMORY_LENGTH
            + ",t=" + ITERATION
            + ",p=" + PARALLELISM_INSTANCE
            + "$";

    public static String hash(String password) {
        int length = ALGO.length();
        return getEncoder().encode(password).substring(length);
    }

    private static Argon2PasswordEncoder getEncoder() {
        return new Argon2PasswordEncoder(SALT_LENGTH, HASH_LENGTH, PARALLELISM_INSTANCE, MEMORY_LENGTH, ITERATION);
    }


    public static boolean verify(String originalPassword, String storedPassword) {
        CharSequence rowPassword = originalPassword;
        String encodedPassword = ALGO + storedPassword;
        return getEncoder().matches(rowPassword, encodedPassword);
    }
}


