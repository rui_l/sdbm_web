package com.beer.jeeprojectsdbm.dao;

import com.beer.jeeprojectsdbm.metier.Fabricant;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class FabricantDAO extends DAO<Fabricant, Fabricant, Integer> {
    @Override
    public Fabricant getByID(Integer id) {
        String sqlRequest = "SELECT ID_FABRICANT ,NOM_FABRICANT from FABRICANT where ID_FABRICANT = " + id;
        Fabricant fabricant  ;
        try (Statement statement = connexion.createStatement()) {
            ResultSet rs = statement.executeQuery(sqlRequest);
            if (rs.next()) return new Fabricant(rs.getInt(1), rs.getString(2));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Fabricant> getAll() {
        ArrayList<Fabricant> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_FABRICANT ,NOM_FABRICANT from FABRICANT";
        try (Statement statement = connexion.createStatement()) {
            ResultSet rs = statement.executeQuery(sqlRequest);
            while (rs.next()) {
                liste.add(new Fabricant(rs.getInt(1), rs.getString(2)));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Fabricant> getLike(Fabricant objet) {
        String sqlCommand = "SELECT ID_FABRICANT ,NOM_FABRICANT from FABRICANT where NOM_FABRICANT like '%" + objet.getLibelle() + "%'";
        ArrayList<Fabricant> liste = new ArrayList<>();
        try (Statement statement = connexion.createStatement()) {
            ResultSet rs = statement.executeQuery(sqlCommand);
            while (rs.next()) {
                liste.add(new Fabricant(rs.getInt(1), rs.getString(2)));
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Fabricant objet) {
        return false;
    }

    @Override
    public boolean update(Fabricant object) {
        return false;
    }

    @Override
    public boolean delete(Fabricant object) {
        return false;
    }
}
