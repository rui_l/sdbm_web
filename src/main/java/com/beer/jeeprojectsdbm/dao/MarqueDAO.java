package com.beer.jeeprojectsdbm.dao;

import com.beer.jeeprojectsdbm.metier.Fabricant;
import com.beer.jeeprojectsdbm.metier.Marque;
import com.beer.jeeprojectsdbm.metier.Pays;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MarqueDAO extends DAO<Marque, Marque, Integer> {
    @Override
    public Marque getByID(Integer id) {
        Marque marque;
        String sqlRequest = "SELECT ID_MARQUE, NOM_MARQUE, ID_PAYS, ID_FABRICANT FROM MARQUE where ID_MARQUE = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                marque = new Marque(resultSet.getInt(1), resultSet.getString(2));
                if (resultSet.getInt(3) != 0)
                    marque.setPays(DaoFactory.getPaysDAO().getByID(resultSet.getInt(3)));
                Integer idFabricant = resultSet.getInt(4);
                if ((idFabricant != null) && (idFabricant != 0)) {
                    marque.setFabricant(DaoFactory.getFabricantDAO().getByID(resultSet.getInt(4)));
                }
                return marque;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Marque> getAll() {
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_MARQUE,NOM_MARQUE, ID_PAYS, ID_FABRICANT from MARQUE";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Marque marque = new Marque(resultSet.getInt(1), resultSet.getString(2));
                if (resultSet.getInt(3) != 0)
                    marque.setPays(DaoFactory.getPaysDAO().getByID(resultSet.getInt(3)));
                Integer idFabricant = resultSet.getInt(4);
                if (idFabricant != null && idFabricant != 0)
                    marque.setFabricant(DaoFactory.getFabricantDAO().getByID(resultSet.getInt(4)));
                liste.add(marque);
            }
            resultSet.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Marque> getLike(Marque objet) {
        return null;
    }

    @Override
    public boolean insert(Marque objet) {
        return false;
    }

    @Override
    public boolean update(Marque object) {
        return false;
    }

    @Override
    public boolean delete(Marque object) {
        return false;
    }

    public List<Marque> getByMaker(Fabricant selectedMaker) {
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_MARQUE,NOM_MARQUE, ID_PAYS, ID_FABRICANT from MARQUE WHERE ID_FABRICANT = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest)) {
            preparedStatement.setInt(1, selectedMaker.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Marque marque = new Marque(resultSet.getInt(1), resultSet.getString(2), selectedMaker);
                if (resultSet.getInt(3) != 0)
                    marque.setPays(DaoFactory.getPaysDAO().getByID(resultSet.getInt(3)));
                liste.add(marque);
            }
            resultSet.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    public List<Marque> getByCountry(Pays selectedCountry) {
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_MARQUE,NOM_MARQUE, ID_PAYS, ID_FABRICANT from MARQUE WHERE ID_PAYS = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest)) {
            preparedStatement.setInt(1, selectedCountry.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Marque marque = new Marque(resultSet.getInt(1), resultSet.getString(2), selectedCountry);
                Integer idFabricant = resultSet.getInt(4);
                if (idFabricant !=null && idFabricant != 0)
                    marque.setFabricant(DaoFactory.getFabricantDAO().getByID(resultSet.getInt(4)));
                liste.add(marque);
            }
            resultSet.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    public List<Marque> getByCountryAndMaker(Pays selectedCountry, Fabricant selectedMaker) {
        ArrayList<Marque> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_MARQUE,NOM_MARQUE, ID_PAYS, ID_FABRICANT from MARQUE WHERE ID_PAYS = ? AND ID_FABRICANT = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest)) {
            preparedStatement.setInt(1, selectedCountry.getId());
            preparedStatement.setInt(2, selectedMaker.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Marque marque = new Marque(resultSet.getInt(1), resultSet.getString(2), selectedCountry, selectedMaker);
                liste.add(marque);
            }
            resultSet.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }
}
