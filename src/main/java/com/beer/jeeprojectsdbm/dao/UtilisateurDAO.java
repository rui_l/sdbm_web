package com.beer.jeeprojectsdbm.dao;

import com.beer.jeeprojectsdbm.metier.Utilisateur;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class UtilisateurDAO extends DAO<Utilisateur, Utilisateur, Integer>{
    @Override
    public Utilisateur getByID(Integer integer) {
        return null;
    }

    @Override
    public ArrayList<Utilisateur> getAll() {
        return null;
    }

    @Override
    public ArrayList<Utilisateur> getLike(Utilisateur objet) {
        return null;
    }

    @Override
    public boolean insert(Utilisateur objet) {
        return false;
    }

    @Override
    public boolean update(Utilisateur object) {
        return false;
    }

    @Override
    public boolean delete(Utilisateur object) {
        return false;
    }

    public Utilisateur getByLogin(String login) {
        Utilisateur utilisateur = new Utilisateur();
        String sqlRequest = "SELECT ID_USER, LOGIN_USER, PASSWORD_USER, EMAIL_USER, ROLE_USER FROM UTILISATEUR WHERE LOGIN_USER = ?";
        try (PreparedStatement preparedStatement = connexion.prepareStatement(sqlRequest, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                utilisateur.setLogin(resultSet.getString(2));
                utilisateur.setPassword(resultSet.getString(3));
                if (resultSet.getString(4) != null && resultSet.getString(4).trim().length() != 0) {
                    utilisateur.setEmail(resultSet.getString(4));
                }
                utilisateur.setRole(resultSet.getString(5));
                return utilisateur;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
