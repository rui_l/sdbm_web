package com.beer.jeeprojectsdbm.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class SDBMConnect2 {
    // Declare the JDBC objects.
    private static Connection connexion;

    private SDBMConnect2() {

    }

    public static Connection getInstance() {
        if (connexion == null) {
            try {
//                String dbURL = "jdbc:sqlserver://127.0.0.1:1435;databaseName=SDBM;encrypt=false";
                String dbURL = "jdbc:sqlserver://localhost:1435;databaseName=SDBM;encrypt=false";
                String user = "dev";
                String pass = "dev@123456";
                connexion = DriverManager.getConnection(dbURL, user, pass);
                System.out.println("Connexion établie ! ");//cette connection ne marche pas.
            }

            // Handle any errors that may have occurred.
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connexion;
    }
}
