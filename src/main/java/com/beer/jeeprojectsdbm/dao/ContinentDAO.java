package com.beer.jeeprojectsdbm.dao;

import com.beer.jeeprojectsdbm.metier.Continent;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ContinentDAO extends DAO<Continent, Continent, Integer> {
    @Override
    public Continent getByID(Integer id) {
        String sqlRequest =
                "SELECT ID_CONTINENT, NOM_CONTINENT FROM CONTINENT WHERE ID_CONTINENT="+id;

        try (Statement statement = connexion.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sqlRequest);
            if (resultSet.next())
                return new Continent(resultSet.getInt(1), resultSet.getString(2));
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Continent> getAll() {
        ArrayList<Continent> liste = new ArrayList<>();
        String sqlRequest =
                "SELECT ID_CONTINENT, NOM_CONTINENT FROM CONTINENT";
        try (Statement statement= connexion.createStatement()){
            ResultSet resultSet=statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                liste.add(new Continent(resultSet.getInt(1),resultSet.getString(2)));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public ArrayList<Continent> getLike(Continent objet) {
        ArrayList<Continent> liste = new ArrayList<>();
        String sqlRequest = "SELECT ID_CONTINENT, NOM_CONTINENT FROM CONTINENT WHERE NOM_CONTINENT LIKE '%" + objet.getLibelle()+"%'";
        try(Statement statement=connexion.createStatement()) {
            ResultSet resultSet=statement.executeQuery(sqlRequest);
            while (resultSet.next()) {
                liste.add(new Continent(resultSet.getInt(1),resultSet.getString(2)));
            }
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Continent objet) {
        //à faire
        return false;
    }

    @Override
    public boolean update(Continent object) {
        //à faire
        return false;
    }

    @Override
    public boolean delete(Continent object) {
        //à faire
        return false;
    }
}
