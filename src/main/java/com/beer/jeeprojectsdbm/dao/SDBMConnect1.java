package com.beer.jeeprojectsdbm.dao;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import java.sql.Connection;

public class SDBMConnect1 {
    // Declare the JDBC objects.
    private static Connection connexion;

    private SDBMConnect1() {
    }

    public static Connection getInstance() {
        if (connexion == null) {
            try {
                SQLServerDataSource ds = new SQLServerDataSource();
                ds.setServerName("localhost");
                ds.setPortNumber(1435);
                ds.setDatabaseName("SDBM");
                ds.setIntegratedSecurity(false);
				ds.setEncrypt(false);
                ds.setUser("dev");
                ds.setPassword("dev@123456");
                connexion = ds.getConnection();
                System.out.println("Connexion établie avec Connect1 ! ");
            }

            // Handle any errors that may have occurred.
            catch (SQLServerException e) {
                e.printStackTrace();
            }
        }
        return connexion;
    }
}
