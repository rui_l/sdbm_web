package com.beer.jeeprojectsdbm.metier;

public class TypeBiere {
    private Integer id;
    private String libelle;

    public TypeBiere() {
        this.id = 0;
        this.libelle = "";
    }
    public TypeBiere(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }

}
