package com.beer.jeeprojectsdbm.metier;

import java.util.ArrayList;

public class Fabricant {
    private int id;
    private String libelle;
//    private ArrayList<Marque> marques;

    public Fabricant(){
        this.id = 0;
        this.libelle = "";
    }
    public Fabricant(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /*public ArrayList<Marque> getMarques() {
        return marques;
    }

    public void setMarques(ArrayList<Marque> marques) {
        this.marques = marques;
    }*/

    @Override
    public String toString() {
        return libelle;
    }
}
