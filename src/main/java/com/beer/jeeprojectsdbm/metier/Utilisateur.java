package com.beer.jeeprojectsdbm.metier;

public class Utilisateur {
    private String login;
    private String password;
    private String email;
    private String role;
    private boolean valide;

    public Utilisateur() {
        login = "";
        password = "";
        email = "";
        role = "";
        valide = false;
    }

    public Utilisateur(String login, String password) {
        this.login = login;
        this.password = password;
        email = "";
        role = "";
        valide = false;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }
}
