package com.beer.jeeprojectsdbm.metier;

public class Couleur {
    private Integer id;
    private String libelle;

    public Couleur() {
        id = 0;
        libelle = "";
    }
    public Couleur(int id, String nomCouleur) {
        this.id = id;
        this.libelle = nomCouleur;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }

}
