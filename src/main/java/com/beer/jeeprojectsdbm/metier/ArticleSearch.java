package com.beer.jeeprojectsdbm.metier;

import com.beer.jeeprojectsdbm.metier.*;

public class ArticleSearch {
//    private int id;
    private String libelle;
    private int volume;
    private float titrageMin;
    private float titrageMax;
    private Marque marque;
    private Fabricant fabricant;
    private Pays pays;
    private Continent continent;
    private Couleur couleur;
    private TypeBiere typeBiere;
    private int page;
    private int lgPage;
    private int nbTotalDeLignes;

    public ArticleSearch() {
        libelle = "";
        titrageMin = 0.5f;
        titrageMax = 26.0f;
        marque = new Marque();
        fabricant = new Fabricant();
        pays = new Pays();
        continent = new Continent();
        couleur = new Couleur();
        typeBiere = new TypeBiere();
        page = 1;
        lgPage = 0;
        nbTotalDeLignes = 0;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public float getTitrageMin() {
        return titrageMin;
    }

    public void setTitrageMin(float titrageMin) {
        this.titrageMin = titrageMin;
    }

    public float getTitrageMax() {
        return titrageMax;
    }

    public void setTitrageMax(float titrageMax) {
        this.titrageMax = titrageMax;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public Fabricant getFabricant() {
        return fabricant;
    }

    public void setFabricant(Fabricant fabricant) {
        this.fabricant = fabricant;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public TypeBiere getTypeBiere() {
        return typeBiere;
    }

    public void setTypeBiere(TypeBiere typeBiere) {
        this.typeBiere = typeBiere;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLgPage() {
        return lgPage;
    }

    public void setLgPage(int lgPage) {
        this.lgPage = lgPage;
    }

    public int getNbTotalDeLignes() {
        return nbTotalDeLignes;
    }

    public void setNbTotalDeLignes(int nbTotalDeLignes) {
        this.nbTotalDeLignes = nbTotalDeLignes;
    }
}
