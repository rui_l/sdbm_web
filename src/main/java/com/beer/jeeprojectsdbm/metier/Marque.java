package com.beer.jeeprojectsdbm.metier;

public class Marque {
    private Integer id;
    private String libelle;
    private Pays pays;
    private Fabricant fabricant;

    public Marque(){
        this.id = 0;
        this.libelle = "";
        this.pays = new Pays();
        this.fabricant = new Fabricant();
    }
    public Marque(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
        this.pays = new Pays();
        this.fabricant = new Fabricant();
    }
    public Marque(int id, String libelle, Pays pays) {
        this.id = id;
        this.libelle = libelle;
        this.pays = pays ;
        this.fabricant = new Fabricant();
    }
    public Marque(int id, String libelle, Fabricant fabricant) {
        this.id = id;
        this.libelle = libelle;
        this.pays = new Pays() ;
        this.fabricant = fabricant;
    }
    public Marque(int id, String libelle, Pays pays, Fabricant fabricant) {
        this.id = id;
        this.libelle = libelle;
        this.pays = pays ;
        this.fabricant = fabricant;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLibelle() {
        return libelle;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public Pays getPays() {
        return pays;
    }
    public void setPays(Pays pays){
        this.pays = pays;
    }
    public Fabricant getFabricant() {
        return fabricant;
    }
    public void setFabricant(Fabricant fabricant) {
        this.fabricant = fabricant;
    }

    @Override
    public String toString() {
        return libelle;
    }

}
