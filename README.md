A Jakarta EE project with JSF
using PrimeFaces

For example, a web page to research a product:
![beers](src/main/webapp/resources/img/beer.PNG)

Website visitor can log in, log out, choose from ComboBoxes, etc.
![choose from ComboBox](src/main/webapp/resources/img/rendu_page_biere.png)
